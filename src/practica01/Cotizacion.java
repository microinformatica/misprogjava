/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica01;

/**
 *
 * @author abrilvelazquezledezma
 */
public class Cotizacion {
    private int folio;
    private String descripcion;
    private float porPagoInicial;
    private float precio;
    private int plazos;

    public Cotizacion(int folio, String descripcion, float porPagoInicial, float precio, int plazos) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.porPagoInicial = porPagoInicial;
        this.precio = precio;
        this.plazos = plazos;
    }

    public Cotizacion() {
        
        this.folio = 0;
        this.descripcion = "";
        this.porPagoInicial = 0.0f;
        this.precio = 0.0f;
        this.plazos = 0;
        
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPorPagoInicial() {
        return porPagoInicial;
    }

    public void setPorPagoInicial(float porPagoInicial) {
        this.porPagoInicial = porPagoInicial;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }
    
    
    public float calcularPagoInicial(){
    float pagoInicial =0;
    pagoInicial = this.precio *(this.porPagoInicial/100);
    return pagoInicial;
    
    }
    
    public float calcularTotalFin(){
    float totalFin=0;
    
    totalFin = this.precio-this.calcularPagoInicial();
    return totalFin;
    }
    
    
    public float calcularPagoMensual(){
     float pagoMensual =0;
     pagoMensual = this.calcularTotalFin()/this.plazos;
     return pagoMensual;
     
    
    
    }
    
  public void imprimir(){
 System.out.println("otra cosa");
  }
    
}
